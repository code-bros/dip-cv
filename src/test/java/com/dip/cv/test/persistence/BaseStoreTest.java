package com.dip.cv.test.persistence;

import com.dip.cv.test.BaseTest;
import org.mockito.Mock;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;

public abstract class BaseStoreTest extends BaseTest {
    @Mock
    @Produces
    protected EntityManager em;
    
    // not relevant in tests as thet em is mocked too
    @Mock
    @Produces
    protected CriteriaBuilder cb = null;
}
