package com.dip.cv.test.persistence;

import com.dip.cv.models.Widget;
import com.dip.cv.persistence.PropertyStore;
import com.dip.cv.persistence.WidgetStore;
import com.dip.cv.persistence.impl.WidgetStoreImpl;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import org.jglue.cdiunit.AdditionalClasses;
import static org.junit.Assert.*;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

@AdditionalClasses({ WidgetStoreImpl.class })
public class WidgetStoreTest extends BaseStoreTest {

    @Captor
    private ArgumentCaptor<Widget> argumentCaptor;
    
    // not relevant in this test
    @Mock
    @Produces
    private PropertyStore propStore;

    @Inject
    private WidgetStore store;

    /**
     * Test whether the store sends the correct widget to the entity manager when persisting new instance
     *
     * @throws Exception
     */
    @Test
    public void createSuccess() throws Exception {
        Widget widget = new Widget();
        widget.setName("Test Widget");

        // mock em to clone the widget to keep original for assertion
        when(em.merge(widget)).thenReturn((Widget) widget.clone());
        
        this.store.create(widget);

        // make sure the merge on the EntityManager is executed only once
        verify(this.em, atMost(1)).merge(argumentCaptor.capture());

        // get the captured instance that was passed from store to entity manager when merge was called
        Widget innerWidget = argumentCaptor.getValue();

        // assert correct instance was passed to the merge
        assertNotNull(innerWidget);
        assertNull(innerWidget.getId());
        assertNull(innerWidget.getCreatedOn());
        assertNull(innerWidget.getModifiedOn());
        assertEquals(widget.getName(), innerWidget.getName());
    }
}
