package com.dip.cv.test.services;

import com.dip.cv.models.Widget;
import com.dip.cv.persistence.WidgetStore;
import com.dip.cv.services.WidgetService;
import com.dip.cv.services.impl.WidgetServiceImpl;
import com.dip.cv.test.BaseTest;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import org.jglue.cdiunit.AdditionalClasses;
import org.mockito.ArgumentCaptor;
import static org.junit.Assert.*;
import org.junit.Test;
import org.mockito.*;
import static org.mockito.Mockito.*;

@AdditionalClasses({ WidgetServiceImpl.class })
public class WidgetServiceTest extends BaseTest {

    @Captor
    private ArgumentCaptor<Widget> argumentCaptor;
    
    @Mock
    @Produces
    protected WidgetStore store;
    
    @Inject
    private WidgetService service;
    
    /**
     * Tests the add method in the widget service
     *
     * @throws Exception
     */
    @Test
    public void addSuccess() throws Exception {
        Widget widget = new Widget();
        widget.setName("Test Widget");

        service.add(widget);

        // make sure the service calls the store create method
        verify(this.store, atMost(1)).create(argumentCaptor.capture());

        Widget innerWidget = argumentCaptor.getValue();

        // assert that the values were not modified inside the service when the instance was passed to the store
        assertEquals(widget.getName(), innerWidget.getName());
    }
}
