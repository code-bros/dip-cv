package com.dip.cv.exceptions;

public abstract class BaseException extends Exception {
    protected final String code;

    public BaseException(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
