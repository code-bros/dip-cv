package com.dip.cv.exceptions;

public class NoPermissionException extends BaseException {
    public static final String CODE = "E-403";

    public NoPermissionException() {
        super(CODE);
    }
}
