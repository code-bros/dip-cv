package com.dip.cv.exceptions;

public class RecordNotFoundException extends BaseException {
    public static final String CODE = "E-404";

    public RecordNotFoundException() {
        super(CODE);
    }
}
