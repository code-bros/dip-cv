package com.dip.cv.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@MappedSuperclass
public abstract class Base<T extends Base> implements Serializable, Cloneable {
    public static final String ID = "id";
    public static final String CREATED_ON = "created_on";
    public static final String MODIFIED_ON = "modified_on";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = Base.ID)
    protected Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = Base.CREATED_ON)
    protected Date createdOn;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = Base.MODIFIED_ON)
    protected Date modifiedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    @PrePersist
    private void prePersist() {
        createdOn = new Date();
        modifiedOn = new Date();
    }

    @PreUpdate
    private void preUpdate() {
        modifiedOn = new Date();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            Base clone = this.getClass().newInstance();
            clone.setId(id);
        
            if (createdOn != null) {
                clone.setCreatedOn((Date) createdOn.clone());
            }

            if (modifiedOn != null) {
                clone.setModifiedOn((Date) modifiedOn.clone());
            }
            
            return clone;
        } catch(InstantiationException | IllegalAccessException e) {
            throw new CloneNotSupportedException(e.getMessage());
        }
    }
}
