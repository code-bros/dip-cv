package com.dip.cv.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = User.TABLE_NAME, uniqueConstraints = {
    @UniqueConstraint(columnNames = { User.NAME })
})
@NamedQueries({
    @NamedQuery(name = User.GET_BY_NAME, query = "SELECT user FROM User user WHERE user.name = :" + User.NAME)
})
public class User extends EntityBase<User> {
    public static final String TABLE_NAME = "dip_user";
    public static final String NAME = "name";

    public static final String GET_BY_NAME = "User.getByName";

    @NotNull
    @Column(name = User.NAME)
    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        User clone = (User) super.clone();
        clone.setName(name);
        return clone;
    }
}
