package com.dip.cv.models;

import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import org.hibernate.annotations.Filter;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = Item.TYPE)
@Table(name = Item.TABLE_NAME, uniqueConstraints = {
    @UniqueConstraint(columnNames = { Item.NAME, Item.TYPE })
})
@FilterDef(name = Item.OWNER_FILTER, parameters = {
    @ParamDef(name=Item.OWNER, type="long" )
})
@Filter(name = Item.OWNER_FILTER, condition = Item.OWNER + " = :" + Item.OWNER)
@NamedQueries({
    @NamedQuery(name = Item.GET_PREFIXED_PATH, query = "SELECT item FROM Item item WHERE item.path LIKE :" + Item.PATH + " || '%' ORDER BY item." + Item.DEPTH + " ASC"),
    @NamedQuery(name = Item.DELETE_MULTIPLE, query = "DELETE FROM Item item WHERE item.id IN :" + Item.ID)
})
public abstract class Item<T extends Item> extends EntityBase<T> {
    public static final String TABLE_NAME = "dip_item";
    public static final String NAME = "name";
    public static final String PARENT = "parent_id";
    public static final String OWNER = "owner_id";
    public static final String DEPTH = "depth";
    public static final String PATH = "path";
    public static final String TYPE = "item_type";
    public static final String OWNER_FILTER = "ownerFilter";

    /**
     * Named query that retrieves all items with provided prefix path. Sorts by item depth in the tree
     *
     * path: The prefix path that items should contain
     */
    public static final String GET_PREFIXED_PATH = "Item.getPrefixedPath";

    /**
     * Named query to delete multiple items at once
     *
     * id: List of item ids to remove
     */
    public static final String DELETE_MULTIPLE = "Item.deleteMultiple";

    @NotNull
    @Column(name = Item.NAME)
    protected String name;

    @ManyToOne
    @JoinColumn(name = Item.PARENT, referencedColumnName = Container.ID)
    protected Container parent;
    
    @NotNull
    @ManyToOne
    @JoinColumn(name = Item.OWNER, referencedColumnName = User.ID)
    protected User owner;

    @NotNull
    @Column(name = Item.DEPTH)
    private Integer depth;
    
    @NotNull
    @Column(name = Item.PATH)
    private String path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Optional<Container> getParent() {
        return Optional.ofNullable(parent);
    }

    public void setParent(Container parent) {
        this.parent = parent;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Integer getDepth() {
        return depth;
    }
    
    public void setDepth(Integer depth) {
        this.depth = depth;
    }
    
    public String getPath() {
        return path;
    }
    
    public void setPath(String path) {
        this.path = path;
    }

    @PreUpdate
    @PrePersist
    private void setDepthAndPath() {
        depth = 0;
        path = "/" + name;
        
        if (parent != null) {
            depth = parent.getDepth() + 1;
            path = parent.getPath() + path;
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Item clone = (Item) super.clone();
        clone.setName(name);
        clone.setDepth(depth);
        clone.setPath(path);
        
        if (parent != null) {
            clone.setParent((Container) parent.clone());
        }
        
        if (owner != null) {
            clone.setOwner((User) owner.clone());
        }
        
        return clone;
    }
}
