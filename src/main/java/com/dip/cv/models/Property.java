package com.dip.cv.models;

import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = Property.TABLE_NAME, uniqueConstraints = {
    @UniqueConstraint(columnNames = { Property.NAME, Property.RECORD_ID, Property.RECORD_TYPE })
})
@NamedQueries({
    @NamedQuery(name = Property.GET_QUERY, query = "SELECT prop FROM Property prop WHERE prop.recordId  = :" + Property.RECORD_ID + " AND prop.recordType = :" + Property.RECORD_TYPE + " AND prop.name = :" + Property.NAME),
    @NamedQuery(name = Property.GET_ALL_QUERY, query = "SELECT prop FROM Property prop WHERE prop.recordId = :" + Property.RECORD_ID + " AND prop.recordType = :" + Property.RECORD_TYPE),
    @NamedQuery(name = Property.GET_ALL_MULTIPLE_QUERY, query = "SELECT prop FROM Property prop WHERE prop.recordId IN :" + Property.RECORD_ID + " AND prop.recordType IN :" + Property.RECORD_TYPE),
    @NamedQuery(name = Property.DELETE_ALL_QUERY ,query = "DELETE FROM Property prop WHERE prop.recordId = :" + Property.RECORD_ID + " AND prop.recordType = :" + Property.RECORD_TYPE),
    @NamedQuery(name = Property.DELETE_MULTIPLE_QUERY, query = "DELETE FROM Property prop WHERE prop.recordId IN :" + Property.RECORD_ID + " AND prop.recordType IN :" + Property.RECORD_TYPE)
})
public class Property extends Base<Property> {
    public static final String TABLE_NAME = "dip_property";
    public static final String NAME = "name";
    public static final String VALUE = "_value";
    public static final String DESCRIPTION = "description";
    public static final String CATEGORY = "category";
    public static final String TYPE = "_type";
    public static final String RECORD_ID = "record_id";
    public static final String RECORD_TYPE = "record_type";

    /**
     * Named query that retrieves single property for a given record
     *
     * recordId: The id of the record too lookup properties
     * recordType: The name of the record model (e.g. item, user)
     * name: The name of the property
     */
    public static final String GET_QUERY = "Property.get";

    /**
     * Named query that retrieves all properties for a given record
     *
     * recordId: The id of the record too lookup properties
     * recordType: The name of the record model (e.g. item, user)
     */
    public static final String GET_ALL_QUERY = "Property.getAll";

    /**
     * Named query that retrieves all properties for multiple records
     *
     * recordId: The list of ids for the records to lookup properties
     * recordType: The name of the record model (e.g. item, user). Can only load properties for similar record types
     */
    public static final String GET_ALL_MULTIPLE_QUERY = "Product.getAllMultiple";

    /**
     * Named query that deletes all properties for a given record
     *
     * recordId: The id of the record too delete props for
     * recordType: The name of the record model (e.g. item, user)
     */
    public static final String DELETE_ALL_QUERY = "Property.deleteAll";

    public static final String DELETE_MULTIPLE_QUERY = "Property.deleteMultiple";

    @NotNull
    @Column(name = Property.NAME)
    private String name;

    @NotNull
    @Column(name = Property.VALUE)
    private String value;

    @Column(name = Property.DESCRIPTION)
    private String description;

    @Column(name = Property.CATEGORY)
    protected String category;

    @Column(name = Property.TYPE)
    protected String type;

    @Column(name = Property.RECORD_ID)
    protected Long recordId;

    @Column(name = Property.RECORD_TYPE)
    protected String recordType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    @PreUpdate
    @PrePersist
    private void setDefaults() {
        if (Objects.isNull(category)) {
            category = "generic";
        }

        if (Objects.isNull(type)) {
            type = "string";
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Property) {
            Property compareTo = (Property) obj;
            return name.equals(compareTo.getName()) &&
                    recordId.equals(compareTo.getRecordId()) &&
                    recordType.equals(compareTo.getRecordType());
        } else {
            return false;
        }
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(name + recordId + recordType);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Property clone = (Property) super.clone();
        clone.setName(name);
        clone.setValue(value);
        clone.setType(type);
        clone.setCategory(category);
        clone.setDescription(description);
        clone.setRecordId(recordId);
        clone.setRecordType(recordType);
        return clone;
    }
}
