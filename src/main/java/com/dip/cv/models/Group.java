package com.dip.cv.models;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = Group.TABLE_NAME, uniqueConstraints = {
    @UniqueConstraint(columnNames = { Group.NAME })
})
@NamedQueries({
    @NamedQuery(name = Group.GET_BY_NAME, query = "SELECT group FROM Group group WHERE group.name = :" + Group.NAME)
})
public class Group extends EntityBase<Group> {
    public static final String TABLE_NAME = "dip_group";
    public static final String NAME = "name";
    
    public static final String GET_BY_NAME = "Group.getByName";
    
    @NotNull
    @Column(name = User.NAME)
    private String name;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "dip_users_groups")
    private Set<User> users;
    
    public Group() {
        users = new HashSet<>();
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
    
    public void addUser(User user) {
        users.add(user);
    }
    
    public void removeUser(User user) {
        users.remove(user);
    }
}
