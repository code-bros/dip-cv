package com.dip.cv.models;

import javax.persistence.Entity;
import java.util.*;
import java.util.stream.Collectors;
import javax.persistence.Transient;

@Entity
public abstract class Container<T extends Container> extends Item<T> implements Iterable<Item> {
    @Transient
    protected Collection<Item> children;

    public Container() {
        children = new ArrayList<>();
    }

    public Collection<Item> getChildren() {
        return children;
    }

    public void setChildren(Collection<Item> children) {
        this.children = children;
    }

    /**
     * Builds a tree rooted at this container instance. The descendants list is consisted of items with parent links which
     * allow for the tree to be constructed
     *
     * @param descendants Collection of Items sorted by depth level
     */
    public void buildTree(Collection<Item> descendants) {
        LinkedList<Item> queue = new LinkedList<>();
        queue.push(this);

        while (!queue.isEmpty()) {
            Item current = queue.pop();

            if (current instanceof Container) {
                Collection<Item> children = descendants.stream().filter(item -> {
                    Optional<Container> parent = item.getParent();

                    if (parent.isPresent()) {
                        return parent.get().getId().equals(current.getId());
                    }

                    return false;
                }).collect(Collectors.toList());

                if (!children.isEmpty()) {
                    ((Container) current).setChildren(children);
                    queue.addAll(children);
                }
            }
        }
    }

    /**
     * Flattens a tree of items rooted at this container, including the container itself.
     *
     * @return A collection of items in the whole tree
     */
    public Collection<Item> flattenTree() {
        Collection<Item> instances = new ArrayList<>();
        Iterator<Item> iterator = iterator();
        iterator.forEachRemaining(instances::add);
        return instances;
    }

    @Override
    public Iterator<Item> iterator() {
        return new ContainerIterator(this);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Container clone = (Container) super.clone();
        Collection<Item> childrenClone = new ArrayList<>();
        
        for (Item child : children) {
            childrenClone.add((Item) child.clone());
        }
        
        clone.setChildren(childrenClone);
        return clone;
    }
    
    /**
     * Iterator that uses BFS to iterate all the descendants for a tree rooted at a specific container, including the container itself.
     */
    private final class ContainerIterator implements Iterator<Item> {
        private LinkedList<Item> queue;
        private final Container root;

        public ContainerIterator(Container root) {
            this.root = root;
            queue = new LinkedList<>();
            queue.add(root);
        }

        @Override
        public boolean hasNext() {
            return !queue.isEmpty();
        }

        @Override
        public Item next() {
            Item item = queue.pop();

            if (item instanceof Container) {
                queue.addAll(((Container) item).getChildren());
            }

            return item;
        }
    }
}
