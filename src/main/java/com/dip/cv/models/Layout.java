package com.dip.cv.models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(Layout.DISCRIMINATOR)
public class Layout extends Container<Layout> {
    public static final String DISCRIMINATOR = "LAYOUT";
}
