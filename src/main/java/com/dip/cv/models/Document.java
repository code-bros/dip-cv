package com.dip.cv.models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(Document.DISCRIMINATOR)
public class Document extends Container<Document> {
    public static final String DISCRIMINATOR = "DOCUMENT";
}
