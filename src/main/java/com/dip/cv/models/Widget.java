package com.dip.cv.models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(Widget.DISCRIMINATOR)
public class Widget extends Item<Widget> {
    public static final String DISCRIMINATOR = "WIDGET";
}
