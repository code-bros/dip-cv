package com.dip.cv.models;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@MappedSuperclass
public abstract class EntityBase<T extends EntityBase> extends Base<T> {
    @Transient
    protected Set<Property> properties;

    public EntityBase() {
        properties = new HashSet<>();
    }

    public Set<Property> getProperties() {
        return properties;
    }

    public void setProperties(Set<Property> properties) {
        this.properties = properties;
    }

    public Optional<Property> getProperty(String name) {
        return properties.stream().filter(prop -> prop.getName().equals(name)).findFirst();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        EntityBase clone = (EntityBase) super.clone();
        Set<Property> clonedProps = new HashSet();
        
        for(Property prop : properties) {
            clonedProps.add((Property) prop.clone());
        }
        
        clone.setProperties(clonedProps);
        return clone;
    }
}
