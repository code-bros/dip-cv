package com.dip.cv.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = Template.TABLE_NAME, uniqueConstraints = {
    @UniqueConstraint(columnNames = { Template.NAME })
})
@NamedQueries({
    @NamedQuery(name = Template.GET_BY_NAME, query = "SELECT template FROM Template template WHERE template.name = :" + Template.NAME)
})
public class Template extends EntityBase<Template> {
    public static final String TABLE_NAME = "dip_template";
    public static final String NAME = "name";
    public static final String VIEW = "view";

    public static final String GET_BY_NAME = "Template.getByName";

    @NotNull
    @Column(name = Template.NAME)
    protected String name;

    @Lob
    @Column(name = Template.VIEW)
    protected String view;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Template clone = (Template) super.clone();
        clone.setName(name);
        clone.setView(view);
        return clone;
    }
}
