package com.dip.cv.services;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.Item;

public interface ItemService<T extends Item> extends BaseService<T> {
    T getByName(String name) throws BaseException;
}
