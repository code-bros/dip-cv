package com.dip.cv.services;

import com.dip.cv.models.Layout;

public interface LayoutService extends ContainerService<Layout> {
}
