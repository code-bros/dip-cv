package com.dip.cv.services;

import com.dip.cv.models.Widget;

public interface WidgetService extends ItemService<Widget> {
}
