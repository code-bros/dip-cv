package com.dip.cv.services;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.User;

public interface UserService extends BaseService<User> {
    User getByName(String name) throws BaseException;
}
