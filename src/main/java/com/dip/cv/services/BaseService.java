package com.dip.cv.services;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.Base;
import java.util.Collection;
import java.util.Map;

public interface BaseService<T extends Base> {
    T add(T instance) throws BaseException;

    T get(Long id) throws BaseException;

    Collection<T> get();

    Collection<T> get(Map<String, Object> filters);

    T update(T instance) throws BaseException;

    void delete(T instance) throws BaseException;
}
