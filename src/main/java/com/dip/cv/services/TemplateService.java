package com.dip.cv.services;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.Item;
import com.dip.cv.models.Template;

public interface TemplateService extends BaseService<Template> {
	Template getForItem(Item item) throws BaseException;

	Template getByName(String name) throws BaseException;
}
