package com.dip.cv.services;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.Group;

public interface GroupService extends BaseService<Group> {
    Group getByName(String name) throws BaseException;
}
