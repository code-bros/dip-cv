package com.dip.cv.services;

import com.dip.cv.models.Document;

public interface DocumentService extends ContainerService<Document> {
}
