package com.dip.cv.services;

import com.dip.cv.models.Container;

public interface ContainerService<T extends Container> extends ItemService<T> {
}
