package com.dip.cv.services.impl.shared;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.Base;
import com.dip.cv.persistence.BaseStore;
import com.dip.cv.services.BaseService;
import java.util.Collection;
import java.util.Map;

public abstract class BaseServiceSharedImpl<T extends Base, S extends BaseStore<T>> implements BaseService<T> {
    protected S store;

    public BaseServiceSharedImpl(S store) {
        this.store = store;
    }

    public BaseServiceSharedImpl() {}

    @Override
    public T add(T instance) throws BaseException {
        return store.create(instance);
    }

    @Override
    public T get(Long id) throws BaseException {
        return store.read(id);
    }

    @Override
    public Collection<T> get() {
        return store.read();
    }

    @Override
    public Collection<T> get(Map<String, Object> filters) {
        return store.read(filters);
    }

    @Override
    public T update(T item) throws BaseException {
        return store.update(item);
    }

    @Override
    public void delete(T instance) throws BaseException {
        store.delete(instance);
    }
}
