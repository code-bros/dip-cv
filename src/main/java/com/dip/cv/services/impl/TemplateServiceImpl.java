package com.dip.cv.services.impl;

import java.util.Optional;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.dip.cv.exceptions.BaseException;
import com.dip.cv.exceptions.RecordNotFoundException;
import com.dip.cv.models.Item;
import com.dip.cv.models.Property;
import com.dip.cv.models.Template;
import com.dip.cv.persistence.TemplateStore;
import com.dip.cv.services.TemplateService;
import com.dip.cv.services.impl.shared.BaseServiceSharedImpl;

@Stateless
public class TemplateServiceImpl extends BaseServiceSharedImpl<Template, TemplateStore> implements TemplateService {
    @Inject
    public TemplateServiceImpl(TemplateStore store) {
        super(store);
    }

    public TemplateServiceImpl() {}

    @Override
    public Template getForItem(Item item) throws BaseException {
        Optional<Property> propOpt = item.getProperty("template");
            return getByName(propOpt.orElseThrow(RecordNotFoundException::new).getValue());
    }

    @Override
    public Template getByName(String name) throws BaseException {
        return store.getByName(name);
    }
}
