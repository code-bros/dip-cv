package com.dip.cv.services.impl;

import com.dip.cv.models.Item;
import com.dip.cv.persistence.ItemStore;
import com.dip.cv.services.ItemService;
import com.dip.cv.services.impl.shared.ItemServiceSharedImpl;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class ItemServiceImpl extends ItemServiceSharedImpl<Item, ItemStore<Item>> implements ItemService<Item> {
    @Inject
    public ItemServiceImpl(ItemStore<Item> store) {
        super(store);
    }

    public ItemServiceImpl() {}
}
