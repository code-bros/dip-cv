package com.dip.cv.services.impl.shared;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.Item;
import com.dip.cv.persistence.ItemStore;
import com.dip.cv.services.ItemService;

public abstract class ItemServiceSharedImpl<T extends Item, S extends ItemStore<T>> extends BaseServiceSharedImpl<T, S> implements ItemService<T> {
    public ItemServiceSharedImpl(S store) {
        super(store);
    }

    public ItemServiceSharedImpl() {}

    @Override
    public T getByName(String name) throws BaseException {
        return store.getByName(name);
    }
}
