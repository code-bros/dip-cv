package com.dip.cv.services.impl;

import com.dip.cv.models.Document;
import com.dip.cv.persistence.DocumentStore;
import com.dip.cv.services.DocumentService;
import com.dip.cv.services.impl.shared.ItemServiceSharedImpl;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class DocumentServiceImpl extends ItemServiceSharedImpl<Document, DocumentStore> implements DocumentService {
    @Inject
    public DocumentServiceImpl(DocumentStore store) {
        super(store);
    }

    public DocumentServiceImpl() {}
}
