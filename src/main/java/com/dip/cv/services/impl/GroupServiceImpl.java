package com.dip.cv.services.impl;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.Group;
import com.dip.cv.persistence.GroupStore;
import com.dip.cv.services.GroupService;
import com.dip.cv.services.impl.shared.BaseServiceSharedImpl;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class GroupServiceImpl extends BaseServiceSharedImpl<Group, GroupStore> implements GroupService {

    @Inject
    public GroupServiceImpl(GroupStore store) {
        super(store);
    }

    public GroupServiceImpl() {}

    @Override
    public Group getByName(String name) throws BaseException {
        return store.getByName(name);
    }
}
