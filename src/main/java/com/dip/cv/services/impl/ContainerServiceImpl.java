package com.dip.cv.services.impl;

import com.dip.cv.models.Container;
import com.dip.cv.persistence.ContainerStore;
import com.dip.cv.services.ContainerService;
import com.dip.cv.services.impl.shared.ItemServiceSharedImpl;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class ContainerServiceImpl extends ItemServiceSharedImpl<Container, ContainerStore<Container>> implements ContainerService<Container> {
    @Inject
    public ContainerServiceImpl(ContainerStore<Container> store) {
        super(store);
    }

    public ContainerServiceImpl() {}
}
