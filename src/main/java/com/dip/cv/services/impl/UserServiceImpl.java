package com.dip.cv.services.impl;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.User;
import com.dip.cv.persistence.UserStore;
import com.dip.cv.services.UserService;
import com.dip.cv.services.impl.shared.BaseServiceSharedImpl;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class UserServiceImpl extends BaseServiceSharedImpl<User, UserStore> implements UserService {

    @Inject
    public UserServiceImpl(UserStore store) {
        super(store);
    }

    public UserServiceImpl() {}

    @Override
    public User getByName(String name) throws BaseException {
        return store.getByName(name);
    }
}
