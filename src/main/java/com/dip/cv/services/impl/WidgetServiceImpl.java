package com.dip.cv.services.impl;

import com.dip.cv.models.Widget;
import com.dip.cv.persistence.WidgetStore;
import com.dip.cv.services.WidgetService;
import com.dip.cv.services.impl.shared.ItemServiceSharedImpl;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class WidgetServiceImpl extends ItemServiceSharedImpl<Widget, WidgetStore> implements WidgetService {
    @Inject
    public WidgetServiceImpl(WidgetStore store) {
        super(store);
    }

    public WidgetServiceImpl() {}
}
