package com.dip.cv.services.impl;

import com.dip.cv.models.Layout;
import com.dip.cv.persistence.LayoutStore;
import com.dip.cv.services.LayoutService;
import com.dip.cv.services.impl.shared.ItemServiceSharedImpl;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class LayoutServiceImpl extends ItemServiceSharedImpl<Layout, LayoutStore> implements LayoutService {
    @Inject
    public LayoutServiceImpl(LayoutStore store) {
        super(store);
    }

    public LayoutServiceImpl() {}
}
