package com.dip.cv.persistence;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.Item;

public interface ItemStore<T extends Item> extends EntityStore<T> {
    T getByName(String name) throws BaseException;
}
