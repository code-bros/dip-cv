package com.dip.cv.persistence.producers;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;

public class EntityManagerProducer {
    @Produces
    @PersistenceContext
    private EntityManager em;
    
    @Produces
    @ApplicationScoped
    private CriteriaBuilder produceCriteriaBuilder() {
        return em.getCriteriaBuilder();
    }
}
