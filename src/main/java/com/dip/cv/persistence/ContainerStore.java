package com.dip.cv.persistence;

import com.dip.cv.models.Container;

public interface ContainerStore<T extends Container> extends ItemStore<T> {
    void loadDescendants(T instance);
}
