package com.dip.cv.persistence;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.Base;
import java.util.Collection;
import java.util.Map;

public interface BaseStore<T extends Base> {
    T create(T instance) throws BaseException;

    T read(Long id) throws BaseException;

    Collection<T> read();

    Collection<T> read(Map<String, Object> filters);

    T update(T instance) throws BaseException;

    void delete(T instance) throws BaseException;

    void delete(Collection<T> instances);
}
