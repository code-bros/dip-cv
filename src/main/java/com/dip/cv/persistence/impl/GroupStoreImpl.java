package com.dip.cv.persistence.impl;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.exceptions.RecordNotFoundException;
import com.dip.cv.models.Group;
import com.dip.cv.persistence.GroupStore;
import com.dip.cv.persistence.impl.shared.EntityStoreSharedImpl;
import javax.ejb.Stateless;

@Stateless
public class GroupStoreImpl extends EntityStoreSharedImpl<Group> implements GroupStore {
    public GroupStoreImpl() {
        super(Group.class);
    }
    
    @Override
    public Group getByName(String name) throws BaseException {
        return em.createNamedQuery(Group.GET_BY_NAME, Group.class)
                .setParameter(Group.NAME, name).getResultList().stream()
                .findFirst().orElseThrow(RecordNotFoundException::new);
    }
}
