package com.dip.cv.persistence.impl;

import com.dip.cv.models.Document;
import com.dip.cv.persistence.DocumentStore;
import com.dip.cv.persistence.impl.shared.ContainerStoreSharedImpl;
import javax.ejb.Stateless;

@Stateless
public class DocumentStoreImpl extends ContainerStoreSharedImpl<Document> implements DocumentStore {
    public DocumentStoreImpl() {
        super(Document.class);
    }
}
