package com.dip.cv.persistence.impl.shared;

import com.dip.cv.models.Container;
import com.dip.cv.models.EntityBase;
import com.dip.cv.models.Item;
import com.dip.cv.models.Property;
import com.dip.cv.persistence.ContainerStore;
import com.dip.cv.persistence.PropertyStore;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;

public abstract class ContainerStoreSharedImpl<T extends Container> extends ItemStoreSharedImpl<T> implements ContainerStore<T> {
    @Inject
    private PropertyStore propertyStore;
    
    public ContainerStoreSharedImpl(Class<T> entityType) {
        super(entityType);
    }

    @Override
    public void loadDescendants(T root) {
        Collection<Item> descendants = em.createNamedQuery(Item.GET_PREFIXED_PATH, Item.class)
                .setParameter(Item.PATH, root.getPath())
                .getResultList();
        
        Set<Property> props = propertyStore.getForInstances(descendants);
        merge(descendants, props);
        root.buildTree(descendants);
    }
    
    private void merge(Collection<? extends EntityBase> instances, Set<Property> props) {
        Map<String, Set<Property>> propsMap = props.parallelStream()
                .collect(Collectors.groupingBy(prop -> prop.getRecordId() + "_" + prop.getRecordType(), Collectors.toSet()));
        
        instances.parallelStream().forEach(instance -> {
            instance.setProperties(propsMap.get(instance.getId() + "_" + instance.getClass().getSimpleName().toLowerCase()));
        });
    }
}
