package com.dip.cv.persistence.impl;

import com.dip.cv.models.Container;
import com.dip.cv.persistence.ContainerStore;
import com.dip.cv.persistence.impl.shared.ContainerStoreSharedImpl;
import javax.ejb.Stateless;

@Stateless
public class ContainerStoreImpl extends ContainerStoreSharedImpl<Container> implements ContainerStore<Container> {
    public ContainerStoreImpl() {
        super(Container.class);
    }
}
