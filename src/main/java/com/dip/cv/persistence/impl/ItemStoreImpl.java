package com.dip.cv.persistence.impl;

import com.dip.cv.models.Item;
import com.dip.cv.persistence.ItemStore;
import com.dip.cv.persistence.impl.shared.ItemStoreSharedImpl;
import javax.ejb.Stateless;

@Stateless
public class ItemStoreImpl extends ItemStoreSharedImpl<Item> implements ItemStore<Item> {
    public ItemStoreImpl() {
        super(Item.class);
    }
}
