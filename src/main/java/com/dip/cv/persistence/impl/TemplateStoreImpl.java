package com.dip.cv.persistence.impl;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.exceptions.RecordNotFoundException;
import com.dip.cv.models.Template;
import com.dip.cv.persistence.TemplateStore;
import com.dip.cv.persistence.impl.shared.EntityStoreSharedImpl;
import javax.ejb.Stateless;

@Stateless
public class TemplateStoreImpl extends EntityStoreSharedImpl<Template> implements TemplateStore {
    public TemplateStoreImpl() {
        super(Template.class);
    }

    @Override
    public Template getByName(String name) throws BaseException {
        return em.createNamedQuery(Template.GET_BY_NAME, Template.class)
                .setParameter(Template.NAME, name)
                .getResultList().stream()
                .findFirst().orElseThrow(RecordNotFoundException::new);
    }
}
