package com.dip.cv.persistence.impl;

import com.dip.cv.models.Layout;
import com.dip.cv.persistence.LayoutStore;
import com.dip.cv.persistence.impl.shared.ContainerStoreSharedImpl;
import javax.ejb.Stateless;

@Stateless
public class LayoutStoreImpl extends ContainerStoreSharedImpl<Layout> implements LayoutStore {
    public LayoutStoreImpl() {
        super(Layout.class);
    }
}
