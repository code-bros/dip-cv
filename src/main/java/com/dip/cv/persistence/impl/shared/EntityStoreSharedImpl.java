package com.dip.cv.persistence.impl.shared;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.interceptors.LoadPropertiesInterceptor;
import com.dip.cv.models.EntityBase;
import com.dip.cv.persistence.EntityStore;
import com.dip.cv.persistence.PropertyStore;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.Collection;

@Interceptors({
    LoadPropertiesInterceptor.class
})
public abstract class EntityStoreSharedImpl<T extends EntityBase> extends BaseStoreSharedImpl<T> implements EntityStore<T> {
    @Inject
    private PropertyStore propertyStore;

    public EntityStoreSharedImpl(Class<T> entityType) {
        super(entityType);
    }

    /**
     * Creates an instance and all properties provided as composite Set in the instance
     *
     * @param instance
     * @return
     * @throws BaseException
     */
    @Override
    public T create(T instance) throws BaseException {
        T addedInstance = super.create(instance);
        addedInstance.setProperties(instance.getProperties());
        addedInstance.setProperties(propertyStore.createForInstance(addedInstance));
        return addedInstance;
    }

    /**
     * Deletes an instance and all properties owned for the instance
     *
     * @param instance
     * @throws BaseException
     */
    @Override
    public void delete(T instance) throws BaseException {
        super.delete(instance);
        propertyStore.deleteForInstance(instance);
    }

    /**
     * Deletes multiple instances and all owned properties for the instances
     *
     * @param instances
     */
    @Override
    public void delete(Collection<T> instances) {
        super.delete(instances);
        propertyStore.deleteForInstances((Collection<EntityBase>)instances);
    }
}
