package com.dip.cv.persistence.impl;

import com.dip.cv.models.Widget;
import com.dip.cv.persistence.WidgetStore;
import com.dip.cv.persistence.impl.shared.ItemStoreSharedImpl;
import javax.ejb.Stateless;

@Stateless
public class WidgetStoreImpl extends ItemStoreSharedImpl<Widget> implements WidgetStore {
    public WidgetStoreImpl() {
        super(Widget.class);
    }
}
