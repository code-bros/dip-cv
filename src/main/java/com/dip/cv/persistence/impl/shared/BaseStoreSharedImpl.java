package com.dip.cv.persistence.impl.shared;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import com.dip.cv.exceptions.BaseException;
import com.dip.cv.exceptions.RecordNotFoundException;
import com.dip.cv.models.Base;
import com.dip.cv.persistence.BaseStore;

public abstract class BaseStoreSharedImpl<T extends Base> implements BaseStore<T> {
    protected Class<T> entityType;

    @Inject
    protected EntityManager em;
    
    @Inject
    protected CriteriaBuilder cb;

    public BaseStoreSharedImpl(Class<T> entityType) {
       this.entityType = entityType;
    }

    @Override
    public T create(T instance) throws BaseException {
        return em.merge(instance);
    }

    @Override
    public T read(Long id) throws BaseException {
        CriteriaQuery<T> cq = cb.createQuery(entityType);
        Root<T> root = cq.from(entityType);
        cq.where(cb.equal(root.get(Base.ID), id));
        return em.createQuery(cq)
                .getResultList()
                .stream().findFirst()
                .orElseThrow(RecordNotFoundException::new);
    }

    @Override
    public Collection<T> read() {
        CriteriaQuery<T> cq = cb.createQuery(entityType);
        cq.from(entityType);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public Collection<T> read(Map<String, Object> filters) {
        CriteriaQuery<T> cq = cb.createQuery(entityType);
        Root<T> root = cq.from(entityType);
        Predicate predicates[] = filters.entrySet().parallelStream()
                .map(filter -> cb.like(root.get(filter.getKey()), "%" + filter.getValue() + "%"))
                .toArray(Predicate[]::new);
        cq.where(cb.and(predicates));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public T update(T instance) throws BaseException {
        return em.merge(instance);
    }

    @Override
    public void delete(T instance) throws BaseException {
        em.remove(em.merge(instance));
    }

    @Override
    public void delete(Collection<T> instances) {
        Collection<Long> ids = instances.stream().map(instance -> instance.getId()).collect(Collectors.toList());

        if (!ids.isEmpty()) {
            CriteriaDelete<T> cq = cb.createCriteriaDelete(entityType);
            Root<T> root = cq.from(entityType);
            cq.where(root.get(Base.ID).in(ids));
            em.createQuery(cq).executeUpdate();
        }
    }
}
