package com.dip.cv.persistence.impl;

import com.dip.cv.models.EntityBase;
import com.dip.cv.models.Property;
import com.dip.cv.persistence.PropertyStore;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Stateless
public class PropertyStoreImpl implements PropertyStore {
    @Inject
    private EntityManager em;

    @Override
    public Set<Property> createForInstance(EntityBase instance) {
        Set<Property> props = instance.getProperties();

        props.forEach(prop -> {
            prop.setRecordId(instance.getId());
            prop.setRecordType(instance.getClass().getSimpleName().toLowerCase());
        });

        return props.stream().map(property -> em.merge(property)).collect(Collectors.toSet());
    }

    @Override
    public Set<Property> getForInstance(EntityBase instance) {
        return em.createNamedQuery(Property.GET_ALL_QUERY, Property.class)
                .setParameter(Property.RECORD_ID, instance.getId())
                .setParameter(Property.RECORD_TYPE, instance.getClass().getSimpleName().toLowerCase())
                .getResultList().stream().collect(Collectors.toSet());
    }

    @Override
    public Set<Property> getForInstances(Collection<? extends EntityBase> instances) {
        Collection<Long> ids = new ArrayList<>();
        Collection<String> types = new ArrayList<>();

        instances.forEach(instance -> {
            ids.add(instance.getId());
            types.add(instance.getClass().getSimpleName().toLowerCase());
        });
        
        return em.createNamedQuery(Property.GET_ALL_MULTIPLE_QUERY, Property.class)
                .setParameter(Property.RECORD_ID, ids)
                .setParameter(Property.RECORD_TYPE, types)
                .getResultList().stream().collect(Collectors.toSet());
    }

    @Override
    public void deleteForInstance(EntityBase instance) {
        em.createNamedQuery(Property.DELETE_ALL_QUERY)
                .setParameter(Property.RECORD_ID, instance.getId())
                .setParameter(Property.RECORD_TYPE, instance.getClass().getSimpleName().toLowerCase())
                .executeUpdate();
    }

    @Override
    public void deleteForInstances(Collection<? extends EntityBase> instances) {
        Collection<Long> ids = new ArrayList<>();
        Collection<String> types = new ArrayList<>();

        instances.forEach(instance -> {
            ids.add(instance.getId());
            types.add(instance.getClass().getSimpleName().toLowerCase());
        });

        em.createNamedQuery(Property.DELETE_MULTIPLE_QUERY)
                .setParameter(Property.RECORD_ID, ids)
                .setParameter(Property.RECORD_TYPE, types)
                .executeUpdate();
    }
}
