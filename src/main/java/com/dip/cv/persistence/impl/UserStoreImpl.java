package com.dip.cv.persistence.impl;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.exceptions.RecordNotFoundException;
import com.dip.cv.models.User;
import com.dip.cv.persistence.UserStore;
import com.dip.cv.persistence.impl.shared.EntityStoreSharedImpl;
import javax.ejb.Stateless;

@Stateless
public class UserStoreImpl extends EntityStoreSharedImpl<User> implements UserStore {
    public UserStoreImpl() {
        super(User.class);
    }

    @Override
    public User getByName(String name) throws BaseException {
        return em.createNamedQuery(User.GET_BY_NAME, User.class)
                .setParameter(User.NAME, name).getResultList().stream()
                .findFirst().orElseThrow(RecordNotFoundException::new);
    }
}
