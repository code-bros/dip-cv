package com.dip.cv.persistence.impl.shared;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.exceptions.RecordNotFoundException;
import com.dip.cv.interceptors.ClosePermissionInterceptor;
import com.dip.cv.interceptors.ContainerSubtreeInterceptor;
import com.dip.cv.interceptors.ItemOwnerInterceptor;
import com.dip.cv.interceptors.OpenPermissionInterceptor;
import com.dip.cv.models.Container;
import com.dip.cv.models.Item;
import com.dip.cv.persistence.ItemStore;
import javax.interceptor.Interceptors;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Interceptors({
    OpenPermissionInterceptor.class,
    ItemOwnerInterceptor.class,
    ContainerSubtreeInterceptor.class,
    ClosePermissionInterceptor.class
})
public abstract class ItemStoreSharedImpl<T extends Item> extends EntityStoreSharedImpl<T> implements ItemStore<T> {
    public ItemStoreSharedImpl(Class<T> entityType) {
        super(entityType);
    }

    @Override
    public T getByName(String name) throws BaseException {
        CriteriaQuery<T> cq = cb.createQuery(entityType);
        Root<T> root = cq.from(entityType);
        cq.where(cb.equal(root.get(Item.NAME), name));
        return em.createQuery(cq).getResultList()
                .stream().findFirst().orElseThrow(RecordNotFoundException::new);
    }

    /**
     * It deletes the instance regularly, and if container it removes all items in the whole tree rooted at this instance
     * by flattening the tree into a list of items that are then batch deleted.
     *
     * @param instance The item to be deleted
     * @throws BaseException
     */
    @Override
    public void delete(T instance) throws BaseException {
        if (instance instanceof Container) {
            super.delete(((Container) instance).flattenTree());
        } else {
            super.delete(instance);
        }
    }
}
