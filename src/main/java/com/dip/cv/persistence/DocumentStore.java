package com.dip.cv.persistence;

import com.dip.cv.models.Document;

public interface DocumentStore extends ContainerStore<Document> {
}
