package com.dip.cv.persistence;

import com.dip.cv.models.EntityBase;
import com.dip.cv.models.Property;
import java.util.Collection;
import java.util.Set;

public interface PropertyStore {
    Set<Property> createForInstance(EntityBase instance);

    Set<Property> getForInstance(EntityBase instance);
    
    Set<Property> getForInstances(Collection<? extends EntityBase> instances);

    void deleteForInstance(EntityBase instance);

    void deleteForInstances(Collection<? extends EntityBase> instances);
}
