package com.dip.cv.persistence;

import com.dip.cv.models.Widget;

public interface WidgetStore extends ItemStore<Widget> {
}
