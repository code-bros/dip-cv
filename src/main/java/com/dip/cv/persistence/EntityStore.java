package com.dip.cv.persistence;

import com.dip.cv.models.EntityBase;

public interface EntityStore<T extends EntityBase> extends BaseStore<T> {
}
