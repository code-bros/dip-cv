package com.dip.cv.persistence;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.Group;

public interface GroupStore extends EntityStore<Group>{
    Group getByName(String name) throws BaseException;
}
