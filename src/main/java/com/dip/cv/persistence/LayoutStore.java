package com.dip.cv.persistence;

import com.dip.cv.models.Layout;

public interface LayoutStore extends ContainerStore<Layout> {
}
