package com.dip.cv.persistence;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.Template;

public interface TemplateStore extends EntityStore<Template> {
    Template getByName(String name) throws BaseException;
}
