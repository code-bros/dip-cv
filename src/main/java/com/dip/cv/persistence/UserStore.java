package com.dip.cv.persistence;

import com.dip.cv.exceptions.BaseException;
import com.dip.cv.models.User;

public interface UserStore extends EntityStore<User> {
    User getByName(String name) throws BaseException;
}
