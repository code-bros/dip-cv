package com.dip.cv.interceptors;

import com.dip.cv.annotations.Principal;
import com.dip.cv.models.Item;
import com.dip.cv.models.User;
import com.dip.cv.persistence.UserStore;
import java.util.Arrays;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 * Sets the owner of the item being added/updated to the persistence store
 */
public class ItemOwnerInterceptor {
    @Inject
    @Principal
    private User user;

    @Inject
    private UserStore userStore;
    
    @AroundInvoke
    public Object setItemOwner(InvocationContext ctx) throws Exception {
        Object params[] = ctx.getParameters();
        
        if (params != null && params.length > 0) {
            // the Principal User is not the whole user instance, we need to get it from the db
            User owner = userStore.read(user.getId());
            
            Arrays.stream(params)
                    .filter(param -> param instanceof Item)
                    .forEach(param -> ((Item)param).setOwner(owner));
        }
        
        return ctx.proceed();
    }
}
