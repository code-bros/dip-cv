package com.dip.cv.interceptors;

import com.dip.cv.models.Container;
import com.dip.cv.persistence.ContainerStore;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class ContainerSubtreeInterceptor {
    @Inject
    private ContainerStore<Container> containerStore;

    @AroundInvoke
    public Object loadSubtree(InvocationContext ctx) throws Exception {
        Object ret = ctx.proceed();
        
        if (ret != null && ret instanceof Container) {
            containerStore.loadDescendants((Container) ret);
        }

        return ret;
    }
}
