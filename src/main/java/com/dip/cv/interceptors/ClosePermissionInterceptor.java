package com.dip.cv.interceptors;

import com.dip.cv.models.Item;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import org.hibernate.Session;

public class ClosePermissionInterceptor {
    @Inject
    private EntityManager em;
    
    @AroundInvoke
    public Object closePermissionFilter(InvocationContext ctx) throws Exception {
        Object ret = ctx.proceed();
        Session session = (Session) em.getDelegate();
        session.disableFilter(Item.OWNER_FILTER);
        return ret;
    }
}
