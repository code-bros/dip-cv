package com.dip.cv.interceptors;

import com.dip.cv.annotations.Principal;
import com.dip.cv.models.Item;
import com.dip.cv.models.User;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import org.hibernate.Filter;
import org.hibernate.Session;

public class OpenPermissionInterceptor {
    @Inject
    private EntityManager em;

    @Inject
    @Principal
    private User user;
    
    @AroundInvoke
    public Object openPermissionFilter(InvocationContext ctx) throws Exception {
        Session session = (Session) em.getDelegate();
        Filter filter = session.enableFilter(Item.OWNER_FILTER);
        filter.setParameter(Item.OWNER, user.getId());
        return ctx.proceed();
    }
}
