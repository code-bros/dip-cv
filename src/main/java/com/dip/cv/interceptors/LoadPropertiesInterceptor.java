package com.dip.cv.interceptors;

import com.dip.cv.models.EntityBase;
import com.dip.cv.persistence.PropertyStore;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class LoadPropertiesInterceptor {
    @Inject
    private PropertyStore propertyStore;

    @AroundInvoke
    public Object loadProperties(InvocationContext ctx) throws Exception {
        Object ret = ctx.proceed();

        // TODO: Load props for instance and all descendants in case the returned instance is container
        if (ret instanceof EntityBase) {
            EntityBase instance = (EntityBase) ret;

            if (instance.getProperties().isEmpty()) {
                instance.setProperties(propertyStore.getForInstance(instance));
            }
        }

        return ret;
    }
}
