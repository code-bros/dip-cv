# Digital Information Platform - CV

This is a DIP core extension module that is used for building the CV application. It offers extension models and services, plus REST endpoints specific to the CV application.
Further in this document we will refer to the CV models and services as a business models and services because they support specific domain, that is an application for building CVs.

![cv architecture](https://drive.google.com/uc?id=0BxAtO_oX1AoXNE9pdnRCR3ZEVlk)

## Model

The business models for the CV are representing the state and relationships between items specific for a CV.
There are currently 3 models that are used to store the meta for a CV document. Namely, the Document, Layout and Widget models.
Currently all the models have the same instance properties inherited from the core models they extend. The types are mostly used to create
separation so that render handling can be easily done. Based on the type and specific properties in the map we can handle the rendering differently.

### com.dip.cv.models.Document extends com.dip.core.model.Container

The Document model represents a single CV document. It extends the Container from the core because it naturally represents a Container
for other Items inside the CV. The children for this Container type are either Layouts or Widgets, which are explained below.
The document is expected to have at least the ```view``` property (inherited from the Item model) that specifies the location for the view file.
This view file is later used in the web implementation to render the Document in a specific way.

### com.dip.cv.models.Layout extends com.dip.core.model.Container

The Layout model is used to define the layout in a Document or another parent Layout. It can be used to create UI Containers for rendering purposes.
The Layout model also can define the location of the view file for rendering by using the ```view``` property.

### com.dip.cv.models.Widget extends com.dip.core.model.Item

The Widget model is the Item that describes a single portion of the CV document. It can be used to define various widget-like mini applications that render
specific section of the CV profile. For e.g. we can have the skills widget that renders a skills matrix for the user. It's the leaf Item in the heirarchy of items
starting from the Document. It cannot contain any children hence why is extending the Item from the core. It can/should also supply the ```view``` property to define
the location of the view file for rendering.

### com.dip.cv.models.Template extends com.dip.core.model.Item

The Template model is the Item that stores the template for UI rendering. It contains the content and the styles/scripts as properties.

## Service

The services are simple enough. They just extend the BaseServiceImpl from the core and implement their respective business service interfaces.
The following services are part of the CV module.

![cv service](https://drive.google.com/uc?id=0BxAtO_oX1AoXbU9mczNzR19FTzQ)

The following services are available.

* **com.dip.cv.services.impl.DocumentServiceImpl**
    * This service is offering CRUD operations for the Document model. The respective interface can be used to extend
    the service with additional business logic specific to the Document model.
* **com.dip.cv.services.LayoutService**
    * This service is offering CRUD operations for the Layout model. The respective interface can be used to extend
    the service with additional business logic specific to the Layout model.
* **com.dip.cv.services.WidgetService**
    * This service is offering CRUD operations for the Widget model. The respective interface can be used to extend
    the service with additional business logic specific to the Widget model.
* **com.dip.cv.services.TemplateService**
    * This service is offering CRUD operations for the Template model. The respective interface can be used to extend 
    the service with additional business logic specific to the Template model.